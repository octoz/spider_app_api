#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from model_utils import load_model, load_model_inputs_from_s3_bucket, \
    make_prediction
from image_preprocess import process_new_image
import argparse


def main(bucket_name: str, path_to_image: str, path_to_model_json: str, path_to_model_h5: str) -> tuple:

    # load + process image
    image = process_new_image(bucket_name=bucket_name, path_to_image=path_to_image)

    # load model inputs - json content and weights file path
    json, weights_path = load_model_inputs_from_s3_bucket(bucket_name=bucket_name,
                                                          path_to_json=path_to_model_json,
                                                          path_to_h5=path_to_model_h5)
    # load model
    model = load_model(json_content=json, model_weights_path=weights_path)

    # predict label + confidence value from image using loaded model
    return make_prediction(model=model, image=image)


if __name__ == '__main__':

    PARSER: argparse.ArgumentParser = argparse.ArgumentParser()
    PARSER.add_argument("bucket",
                        help="name of S3 bucket containing files.")
    PARSER.add_argument("image_key",
                        help="path to image to be processed.")
    PARSER.add_argument("json_path",
                        help="path to model layers json file.")
    PARSER.add_argument("h5_path",
                        help="path to model weights h5 file.")
    ARGS: argparse.Namespace = PARSER.parse_args()

    BUCKET_NAME = ARGS.bucket
    IMAGE_KEY = ARGS.image_key
    JSON_PATH = ARGS.json_path
    H5_PATH = ARGS.h5_path
    predicted_label, confidence = main(BUCKET_NAME, IMAGE_KEY, JSON_PATH, H5_PATH)

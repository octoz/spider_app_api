#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
This script provides methods to load from an S3 bucket and reformat the image for use as an input
to a CNN
"""

import boto3
import numpy as np
import PIL
from PIL import Image
from keras.preprocessing.image import img_to_array


DEFAULT_WIDTH = 224
DEFAULT_HEIGHT = 224


def resize_image(image: PIL.Image.Image, width: int = DEFAULT_WIDTH, height: int = DEFAULT_HEIGHT) -> np.ndarray:
    """
    This methods resize automatically any image in the specified output format and returns as array
    with dimensions = (1, W, H, 3).
    """
    resized_image = image.resize((width, height), PIL.Image.ANTIALIAS)
    resized_image = np.stack([img_to_array(resized_image)])
    return resized_image


def read_s3_image_file_content(bucket_name: str, key: str) -> PIL.Image.Image:
    """
    This method returns an image object from given S3 bucket with specified key (path)
    """
    # TODO close ssl resource
    s3 = boto3.resource('s3')
    bucket = s3.Bucket(bucket_name)
    image_obj = bucket.Object(key)
    response = image_obj.get()
    file_stream = response['Body']
    img = Image.open(file_stream)
    return img


def process_new_image(bucket_name: str, path_to_image: str) -> np.ndarray:
    """
    This method processes an image file from S3 bucket with specified bucket name and image path
    and returns an image object of resized image
    """
    image = read_s3_image_file_content(bucket_name, path_to_image)
    resized_image = resize_image(image)
    return resized_image

import boto3
import logging
from src import main

logger = logging.getLogger()
logger.setLevel(logging.INFO)

s3 = boto3.client('s3')
BUCKET_NAME = "spider-recognition"
PATH_TO_JSON = "model_layers.json"
PATH_TO_H5 = "model_weights.h5"


def lambda_handler(event, _):
    logger.info('EVENT')
    logger.info(event)

    img_key = "Dataset/test/Spider/000cf5859025877f.jpg"
        #event['Records']['s3']['object']['key']

    logger.info("Making prediction for " + img_key)

    predicted_label, predicted_confidence = main(bucket_name=BUCKET_NAME,
                                                 path_to_image=img_key,
                                                 path_to_model_json=PATH_TO_JSON,
                                                 path_to_model_h5=PATH_TO_H5)

    logger.info("Predicted Label: " + predicted_label)
    logger.info("Confidence: " + str(predicted_confidence))

    return predicted_label, predicted_confidence


if __name__ == "__main__":

    lambda_handler("hi", "hi")

#!/usr/bin/env python
# coding: utf-8
"""
This script provides useful methods for machine learning exploration.
"""

from keras.engine.training import Model
from keras.models import model_from_json
import boto3
import tempfile
import numpy as np


def load_json_file_content_from_s3_bucket(bucket_name: str, file_path: str) -> str:
    # TODO close ssl resource
    json_object = boto3.resource('s3').Object(bucket_name, file_path)
    json_content = json_object.get()['Body'].read().decode('utf-8')
    return json_content


def load_json_file_from_local(file_path: str) -> str:
    with open(file_path, 'r') as json_file:
        json_content = json_file.read()
    return json_content


def create_temp_file_path_from_s3_bucket(bucket_name: str, file_path: str) -> str:
    file_object = boto3.resource('s3').Object(bucket_name, file_path)
    tmp = tempfile.NamedTemporaryFile(delete=False)
    # TODO close ssl resource
    with open(tmp.name, 'wb') as f:
        file_object.download_fileobj(f)
    return tmp.name


def load_model_inputs_from_s3_bucket(bucket_name: str, path_to_json: str, path_to_h5: str) -> tuple:
    json_content = load_json_file_content_from_s3_bucket(bucket_name, path_to_json)
    h5_tmp_path = create_temp_file_path_from_s3_bucket(bucket_name, path_to_h5)

    return json_content, h5_tmp_path


def load_model(json_content: str, model_weights_path: str) -> Model:
    """
    Load and reconstruct a model from a json file containing the network and a .h5 file for the
    weights.
    """

    model = model_from_json(json_content)
    model.load_weights(model_weights_path)

    print("Loaded model from disk")
    return model


def make_prediction(model, image: np.ndarray) -> tuple:
    predict_label = model.predict(image)
    label = num_to_class(np.argmax(predict_label, axis=1)[0])
    conf_pred = predict_label[0, np.argmax(predict_label, axis=1)[0]]
    return label, conf_pred


def num_to_class(predicted_class: int) -> str:
    spider_classes = {0: 'black_house_spider',
                      1: 'brown_house_spider',
                      2: 'daddy_long_legs_spider',
                      3: 'funnel_web_spider',
                      4: 'garden_orb_weaver_spider ',
                      5: 'huntsman_spider',
                      6: 'mouse_spider',
                      7: 'recluse_spider',
                      8: 'red_back_spider',
                      9: 'spiderman',
                      10: 'st_andrew_cross_spider',
                      11: 'tarantula_spider',
                      12: 'trap_door_spider',
                      13: 'white_tailed_spider',
                      14: 'wolf_spider'}

    return spider_classes[predicted_class]
